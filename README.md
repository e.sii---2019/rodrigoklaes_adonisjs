## Passo-à-passo para rodar o projeto

- Clonar o projeto

- Rodar o comando "cd to-do-list-adonis" para entrar na pasta do projeto

- Rodar o comando "npm i" para instalar as dependecias

- Criar um arquivo .env baseado no .env.exemplo 

- Rodar o comando o comando "adonis serve --dev" para iniciar o servidor

- Acessar o localhost:3333/tarefas


- ### Link Video
- https://www.youtube.com/watch?v=ZFOCIm1QE5M